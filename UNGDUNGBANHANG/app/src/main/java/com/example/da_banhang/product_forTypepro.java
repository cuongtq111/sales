package com.example.da_banhang;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class product_forTypepro extends AppCompatActivity {
    GridView grdanhsachsanpham ;
    ArrayList<Product_share> mang1 ;
    TextView back_home , tenloai;
    adapter_sanphamtheoloai adapter;
    ImageView theme_type;
    int id = 0 ;
    int page1 = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.);
        setContentView(R.layout.activity_product_for_typepro);
        theme_type = findViewById(R.id.theme_type);
        back_home = findViewById(R.id.back_home);
        tenloai = findViewById(R.id.name_loaisanpham);
        tenloai.setText(getIntent().getStringExtra("tenloai"));
        Picasso.with(getApplicationContext()).load(getIntent().getStringExtra("anhloai")).placeholder(R.mipmap.ic_launcher).into(theme_type);
        back_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        grdanhsachsanpham = findViewById(R.id.grid_sanphamtheoloai);



        layidloai();
        laydulieuve(page1);
        mang1 = new ArrayList<>();

        grdanhsachsanpham.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext() , Detail_product.class);
                intent.putExtra("ma", mang1.get(position).getId());
                intent.putExtra("ten", mang1.get(position).getName_pro_share());
                intent.putExtra("anh", mang1.get(position).getImg_pro_share());
                intent.putExtra("gia", mang1.get(position).getPri_pro_share());
                intent.putExtra("mota", mang1.get(position).getMota());
                intent.putExtra("soluong", mang1.get(position).getStatus_pro_share());
                intent.putExtra("thuonghieu", mang1.get(position).getThuonghieu());
                intent.putExtra("xuatxu", mang1.get(position).getXuatxu());
                startActivity(intent);
            }
        });

    }

    private void laydulieuve(final int page) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String url = Link_page.sanphamtheoloai + String.valueOf(page);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    mang1 = new ArrayList<>();
                    JSONArray dataArray  = object.getJSONArray("Tsanpham");

                    for (int i = 0; i < dataArray.length(); i++) {
                        Product_share playerModel = new Product_share();
                        JSONObject dataobj = dataArray.getJSONObject(i);
                        playerModel.setId(dataobj.getInt("ma"));
                        playerModel.setName_pro_share(dataobj.getString("name"));
                        playerModel.setImg_pro_share(dataobj.getString("img"));
                        playerModel.setMota(dataobj.getString("mota"));
                        playerModel.setPri_pro_share(dataobj.getInt("price"));
                        playerModel.setThuonghieu(dataobj.getString("thuonghieu"));
                        playerModel.setXuatxu(dataobj.getString("xuatxu"));
                        mang1.add(playerModel);
                    }
                    adapter = new adapter_sanphamtheoloai(getApplicationContext() , mang1 ,R.layout.row_sanphamtheoloai);
                    grdanhsachsanpham.setAdapter(adapter);

                }
                catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("abc" ,e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> pagram = new HashMap<String,String>();
                pagram.put("id_loaisp", String.valueOf(id));
                return pagram;
            }
        };
        requestQueue.add(stringRequest);
    }
    public void layidloai(){
        id = getIntent().getIntExtra("maloai", -1);
    }
}
