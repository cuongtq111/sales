package com.example.da_banhang;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Register_v2 extends AppCompatActivity {
    TextView btnlogin, btnregister ;
    EditText name, mail, pass, pass_again;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_v2);
        anhxa();
        final String name_check = name.getText().toString().trim();
        final String mail_check = mail.getText().toString().trim();
        final String pass_check = pass.getText().toString();
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Register_v2.this , Login_v2.class));
            }
        });
        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( name_check.length() < 0 && mail_check.length() < 0 && pass_check.length() < 0){
                    name.setError("Không được để trống!");
                    name.requestFocus();
                    mail.setError("Không được để trống!");
                    mail.requestFocus();
                    pass.setError("Không được để trống!");
                    pass.requestFocus();
                } else {
                    register();
                }


            }
        });
    }

    public  void  anhxa(){
        btnlogin = findViewById(R.id.link_dangnhap);
        name = findViewById(R.id.edit_name);
        mail  = findViewById(R.id.edit_mail);
        pass = findViewById(R.id.edit_pass);
        pass_again = findViewById(R.id.edit_passagain);
        btnregister = findViewById(R.id.btnregister);
    }
    public void register(){
        String url = Link_page.register;

        final String passagain_check = this.pass_again.getText().toString().trim();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String sucess = jsonObject.getString("succes");
                            if(sucess.equals("1")){
                                Intent i = new Intent(Register_v2.this , MainActivity.class);
                                i.putExtra("name1", mail.getText().toString());
                                startActivity(i);
                                Toast.makeText(Register_v2.this, "Đăng kí thành công", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(Register_v2.this, "Đăng kí không thành công", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(Register_v2.this, "Sorry" + e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Register_v2.this, "ABC" + error.toString(), Toast.LENGTH_SHORT).show();

                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String > post = new HashMap<>();
                post.put("name",name.getText().toString());
                post.put("mail",mail.getText().toString());
                post.put("matkhau", pass.getText().toString());
                return post;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }
    protected  boolean checkmail(String check_mail){
        String chechmail = "[a-zA-Z0-9._-]+@[a-z]+.+[a-z]+";
        Pattern patterns = Pattern.compile(chechmail);
        Matcher matcher = patterns.matcher(check_mail);

        return  matcher.matches();
    }
}
