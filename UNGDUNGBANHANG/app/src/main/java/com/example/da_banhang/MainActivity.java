package com.example.da_banhang;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    private long back_;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Ánh xạ từ playout
        BottomNavigationView bottomNavigationView  = findViewById(R.id.bottom_nagation);

        //Code khai báo
        bottomNavigationView.setOnNavigationItemSelectedListener(nav);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentABC, new trangchu_fragment()).commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //Khai báo và ánh xạ menu ra màn hình
        getMenuInflater().inflate(R.menu.search ,menu);
        MenuItem menuItem = menu.findItem(R.id.search);
        return super.onCreateOptionsMenu(menu);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener nav = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            //Chọn từng item trong menu để hiển thị từng trang lên màn hình activity
            Fragment selectitem = null;
            switch (menuItem.getItemId()){
                case R.id.home:
                    selectitem = new trangchu_fragment();
                    break;
                case R.id.kind:
                    selectitem = new theloai_fragment();
                    break;
                case R.id.account:
                    selectitem = new taikhoan_fragment();
                    break;
                default:
                    selectitem = new taikhoan_fragment();
                    break;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentABC, selectitem).commit();
            return true;
        }
    };

}
