package com.example.da_banhang;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class shopping_cart_adapter extends BaseAdapter {
    Context mcontext ;
    ArrayList<shopping_cart_class> manggiohang;
    public static  int giamoinhat = 0;
    public shopping_cart_adapter(Context mcontext, ArrayList<shopping_cart_class> manggiohang) {
        this.mcontext = mcontext;
        this.manggiohang = manggiohang;
    }

    @Override
    public int getCount() {
        return manggiohang.size();
    }

    @Override
    public Object getItem(int i) {
        return manggiohang.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public class Viewholder_cart{
        ImageView anh;
        TextView name, gia;
        Button soluong;
        Button btntang , btngiam , xoahang;
        ListView lihang;
    }
    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
       Viewholder_cart holder = null;
       if(view == null){
           holder = new Viewholder_cart();
           LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
           view = inflater.inflate(R.layout.row_cart, null);

           holder.anh = view.findViewById(R.id.img_cart);
           holder.name = view.findViewById(R.id.name_cart);
           holder.gia = view.findViewById(R.id.gia_cart);
           holder.soluong = view.findViewById(R.id.soluong_cart);
           holder.btntang = view.findViewById(R.id.btntang);
           holder.btngiam = view.findViewById(R.id.btngiam);
           holder.lihang = view.findViewById(R.id.list_shoppingcart);

           view.setTag(holder);
       }else {
           holder = (Viewholder_cart) view.getTag();

       }


       shopping_cart_class cart = (shopping_cart_class) getItem(i);
       holder.name.setMaxLines(2);
       holder.name.setEllipsize(TextUtils.TruncateAt.END);
       holder.name.setText(cart.getTen());
        final DecimalFormat format = new DecimalFormat("###,###,###");
       holder.gia.setText("Giá: " + format.format(cart.getGia()) + " đ");
       Picasso.with(mcontext).load(cart.getAnh()).into(holder.anh);
       holder.soluong.setText("" + cart.getSoluong());
       final int sl1 =  Integer.parseInt(holder.soluong.getText().toString());
        if(sl1 >= 5){
            holder.btntang.setVisibility(view.INVISIBLE);
            holder.btngiam.setVisibility(view.VISIBLE);
        }
        else if(sl1 <= 1){
            holder.btngiam.setVisibility(view.INVISIBLE);
        }
        else if( sl1 > 1){
            holder.btntang.setVisibility(view.VISIBLE);
            holder.btngiam.setVisibility(view.VISIBLE);
        }

        final Viewholder_cart finalHolder = holder;
        holder.btntang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int slmoinhat = Integer.parseInt(finalHolder.soluong.getText().toString()) + 1;
                int slhientai = trangchu_fragment.array_shoppingcart.get(i).getSoluong();
                int giasp = trangchu_fragment.array_shoppingcart.get(i).getGia();
                trangchu_fragment.array_shoppingcart.get(i).setSoluong(slmoinhat);
                giamoinhat =  (giasp * slmoinhat ) / slhientai;
                trangchu_fragment.array_shoppingcart.get(i).setGia(giamoinhat);
                DecimalFormat format1 = new DecimalFormat("###,###,###");
                finalHolder.gia.setText( "Giá: "+ format.format(giamoinhat) + " đ");
                shopping_cart_v2.tinhtien();
                if(slmoinhat >4){
                    finalHolder.btngiam.setVisibility(View.VISIBLE);
                    finalHolder.btntang.setVisibility(View.INVISIBLE);
                    finalHolder.soluong.setText(String.valueOf(slmoinhat));
                }else {
                    finalHolder.btntang.setVisibility(View.VISIBLE);
                    finalHolder.btngiam.setVisibility(View.VISIBLE);
                    finalHolder.soluong.setText(String.valueOf(slmoinhat));
                }
            }
        });
        holder.btngiam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int slmoinhat = Integer.parseInt(finalHolder.soluong.getText().toString()) - 1;
                int slhientai = trangchu_fragment.array_shoppingcart.get(i).getSoluong();
                int giasp = trangchu_fragment.array_shoppingcart.get(i).getGia();
                trangchu_fragment.array_shoppingcart.get(i).setSoluong(slmoinhat);
                giamoinhat =  (giasp * slmoinhat ) / slhientai;
                trangchu_fragment.array_shoppingcart.get(i).setGia(giamoinhat);
                DecimalFormat format1 = new DecimalFormat("###,###,###");
                finalHolder.gia.setText( "Giá: "+ format.format(giamoinhat) + " đ");
                shopping_cart_v2.tinhtien();
                if(slmoinhat <2){
                    finalHolder.btngiam.setVisibility(View.INVISIBLE);
                    finalHolder.btntang.setVisibility(View.VISIBLE);
                    finalHolder.soluong.setText( String.valueOf(slmoinhat));
                }else {
                    finalHolder.btntang.setVisibility(View.VISIBLE);
                    finalHolder.btngiam.setVisibility(View.VISIBLE);
                    finalHolder.soluong.setText(String.valueOf(slmoinhat));
                }
            }
        });

        return view;
    }
}
