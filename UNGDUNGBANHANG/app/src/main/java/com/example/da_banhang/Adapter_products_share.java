package com.example.da_banhang;

import android.content.Context;
import android.content.Intent;
import android.text.Layout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Adapter_products_share extends RecyclerView.Adapter<Adapter_products_share.Viewholder1> {

    ArrayList<Product_share> arrayList_sanpham;
    Context context;

    public Adapter_products_share(ArrayList<Product_share> arrayList_sanpham, Context context) {
        this.arrayList_sanpham = arrayList_sanpham;
        this.context = context;
    }

    @NonNull
    @Override
    public Viewholder1 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view1 = inflater.inflate(R.layout.row_products,parent,false);
        Animation animation = AnimationUtils.loadAnimation(context,R.anim.scale_);
        view1.startAnimation(animation);
        return new Viewholder1(view1);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder1 holder, final int position) {
//        holder.img_pro.setImageResource(arrayList_sanpham.get(position).getImg_pro_share());
        Picasso.with(context)
                .load(arrayList_sanpham.get(position).getImg_pro_share())
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.img_pro);
        holder.txt_namepro.setMaxLines(1);
        holder.txt_namepro.setEllipsize(TextUtils.TruncateAt.END);
        holder.txt_namepro.setText(arrayList_sanpham.get(position).getName_pro_share());
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
        holder.txt_pricepro.setText("Giá: "+decimalFormat.format(arrayList_sanpham.get(position).getPri_pro_share()) + " đ");
        holder.txt_status.setText("Số lượng tồn: "+arrayList_sanpham.get(position).getStatus_pro_share() + " ");
        holder.txtmota.setMaxLines(1);
        holder.txtmota.setEllipsize(TextUtils.TruncateAt.END);
        holder.txtmota.setText("Mô tả: " + arrayList_sanpham.get(position).getMota());


    }

    @Override
    public int getItemCount() {
        return arrayList_sanpham.size();
    }


    public class Viewholder1 extends RecyclerView.ViewHolder{
        ImageView img_pro;
        TextView txt_namepro, txt_pricepro, txtmota, txt_status;
        public Viewholder1(@NonNull View itemView) {
            super(itemView);
            img_pro = itemView.findViewById(R.id.img_pro);
            txt_namepro = itemView.findViewById(R.id.txt_name_pro);
            txt_pricepro =itemView.findViewById(R.id.txt_price);
            txt_status = itemView.findViewById(R.id.txt_status);
            txtmota = itemView.findViewById(R.id.mota);
        }
    }


}
