package com.example.da_banhang;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class adapter_typehome extends RecyclerView.Adapter<adapter_typehome.Viewholder>  {
    ArrayList<loaisanpham_classs> arrayList_loaisanpham;
    Context context;

    public adapter_typehome(ArrayList<loaisanpham_classs> arrayList_loaisanpham, Context context) {
        this.arrayList_loaisanpham = arrayList_loaisanpham;
        this.context = context;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_typehome,parent,false);
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.scale_);
        view.startAnimation(animation);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        holder.txt_nameloaisanpham.setMaxLines(1);
        holder.txt_nameloaisanpham.setEllipsize(TextUtils.TruncateAt.END);
        holder.txt_nameloaisanpham.setText(arrayList_loaisanpham.get(position).getName());
        Picasso.with(context).load(arrayList_loaisanpham.get(position).getAnhloai()).placeholder(R.mipmap.ic_launcher).into(holder.img_loaisanpham);
    }

    @Override
    public int getItemCount() {
        return arrayList_loaisanpham.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder{
        ImageView img_loaisanpham;
        TextView txt_nameloaisanpham;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            img_loaisanpham = itemView.findViewById(R.id.anhtrang_home);
            txt_nameloaisanpham = itemView.findViewById(R.id.tentrang_home);

        }
    }
}
