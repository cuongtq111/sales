package com.example.da_banhang;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class History_product extends AppCompatActivity {
    ArrayList<Product_share> arrayList;
    History_adapter  history_adapter;
    ListView listhistory;
    TextView back_history;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_product);
        listhistory = findViewById(R.id.listhistory);
        Product_share utils = new Product_share();
        arrayList = utils.getFurnitureHistory();
        history_adapter = new History_adapter(getApplicationContext(),arrayList);
        listhistory.setAdapter(history_adapter);
        back_history = findViewById(R.id.back_history);
        back_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
