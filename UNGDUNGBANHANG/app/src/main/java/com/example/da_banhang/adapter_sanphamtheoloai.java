package com.example.da_banhang;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

public class adapter_sanphamtheoloai extends BaseAdapter {
    Context context ;
    List<Product_share> mangsanpham;
    int layout;


    public adapter_sanphamtheoloai(Context context, List<Product_share> mangsanpham, int layout) {
        this.context = context;
        this.mangsanpham = mangsanpham;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return mangsanpham.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        viewholder v_holder;
        if(view == null){
            v_holder = new viewholder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout ,null);
            v_holder.anh = view.findViewById(R.id.img_sanpham);
            v_holder.name = view.findViewById(R.id.txt_tensanpham);
            v_holder.gia = view.findViewById(R.id.txt_gia);
            v_holder.mota = view.findViewById(R.id.txt_mota);
            view.setTag(v_holder);
        }
        else {
            v_holder = (viewholder) view.getTag();
        }

        Product_share sanpham = mangsanpham.get(position);
        Picasso.with(context).load(sanpham.getImg_pro_share()).into(v_holder.anh);

        v_holder.name.setMaxLines(1);
        v_holder.name.setEllipsize(TextUtils.TruncateAt.END);
        v_holder.name.setText(sanpham.getName_pro_share());
        DecimalFormat format  = new DecimalFormat("###,###,###");
        v_holder.gia.setText("Giá:" + format.format(sanpham.getPri_pro_share()) + "đ");
        v_holder.mota.setMaxLines(3);
        v_holder.mota.setEllipsize(TextUtils.TruncateAt.END);
        v_holder.mota.setText( "Mô tả: " + sanpham.getMota());
        return view;
    }

    public class viewholder{
        ImageView anh;
        TextView name, gia, mota ;
    }


}
