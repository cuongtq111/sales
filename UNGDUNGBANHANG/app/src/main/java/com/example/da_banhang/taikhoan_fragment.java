package com.example.da_banhang;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.sdsmdg.harjot.longshadows.shadowutils.Utils;

import java.util.ArrayList;

public class taikhoan_fragment extends Fragment {
    SharedPreferences sharedPreferences ;
    TextView txtuser, ten, btn , logout;
    Button btnsnaphamdaxem;
    SharedPreferences.Editor editor ;
    ArrayList<Product_share> arrayList;
    History_adapter  history_adapter;
    ListView listhistory;
    public void shardulieu(){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = sharedPreferences.edit();
    }
    public taikhoan_fragment(){

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.taikhoan_fragment , container, false);
        logout = view.findViewById(R.id.logout);
        btn = view.findViewById(R.id.btn_cart_tk);
        btn.setText("" + trangchu_fragment.array_shoppingcart.size());
        btnsnaphamdaxem = view.findViewById(R.id.btnsnaphamdaxem);
        txtuser = view.findViewById(R.id.txt_name_user);
        txtuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity() , Login_v2.class));
            }
        });




        final Intent intent = getActivity().getIntent();

        if (intent.getStringExtra("name1") == null){

            logout.setText("");
        }
        else {
            txtuser.setText("TK: " + intent.getStringExtra("name1"));
            txtuser.setClickable(false);
            logout.setText("Đăng xuất");
        }

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("name1" , "");
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity() , shopping_cart_v2.class));
            }
        });

        btnsnaphamdaxem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity() , History_product.class));
            }
        });
        return view;
    }
}
