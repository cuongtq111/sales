package com.example.da_banhang;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

public class Detail_product extends AppCompatActivity {

    TextView ten ,gia, mota , back , xuatxu, thuonghieu;
    Button btnadd;
    ScrollView top;
    ImageView anh;
    //Biến chứa
    int giachitiet =0;
    int machitiet = 0;
    String tenchitiet = "";
    String anhchitiet = "";
    String motachitiet = "";
    int soluongchitiet = 0;
    String xuatxu1 = "";
    String thuonghieu1 = "";

    Bundle  bundle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_product);
        xuatxu = findViewById(R.id.xuatxu);
        thuonghieu = findViewById(R.id.thuonghieu);
        anh  = findViewById(R.id.view1);
        top = findViewById(R.id.top);
        btnadd = findViewById(R.id.btn_addcart);
        ten = findViewById(R.id.ten);
        gia = findViewById(R.id.gia);
        mota = findViewById(R.id.mota);
        back = findViewById(R.id.back_home2);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        laydulieu();
        gandulieuvaomanhinh();

        //Click thêm vào giỏ hàng
        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( trangchu_fragment.array_shoppingcart.size() > 0 ){
                    //Sản phẩm chưa tồn  tại
                    int soluong = 1;
                    boolean tontai = false;
                    for(int i = 0 ; i < trangchu_fragment.array_shoppingcart.size() ; i++){
                        if (trangchu_fragment.array_shoppingcart.get(i).getMa()  ==  machitiet)
                        {
                            trangchu_fragment.array_shoppingcart.get(i).setSoluong(trangchu_fragment.array_shoppingcart.get(i).getSoluong() + soluong);
                            trangchu_fragment.array_shoppingcart.get(i).setGia(trangchu_fragment.array_shoppingcart.get(i).getSoluong() * giachitiet);
                            tontai = true;
                        }
                        //Sản phẩm đã tồn tại set ID để công dồn thêm số lượng
                    }

                    //Nếu chưa tồn tại thì add lại ban đầu
                    if(tontai == false){
                        int sl = 1 ;
                        int giasanpham = giachitiet;
                        trangchu_fragment.array_shoppingcart.add(new shopping_cart_class(machitiet,giachitiet,tenchitiet,anhchitiet,sl,giasanpham));
                    }
                }
                else {
                    int sl = 1 ;
                    int giasanpham = giachitiet  * sl;
                    trangchu_fragment.array_shoppingcart.add(new shopping_cart_class(machitiet,giachitiet,tenchitiet,anhchitiet,sl,giasanpham));
                }
                Toast.makeText(Detail_product.this, "Bạn đã thêm vào món hàng mới", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), shopping_cart_v2.class));
            }
        });
    }

    private void gandulieuvaomanhinh() {
        //Gán vào mang hình chi tiết
        ten.setText(tenchitiet);
        mota.setText(motachitiet);
        DecimalFormat format = new DecimalFormat("###,###,###");
        gia.setText("Giá: "+ format.format(giachitiet) + " đ");
        Picasso.with(getApplicationContext()).load(anhchitiet).into(anh);
        thuonghieu.setText("Thương hiệu: " +thuonghieu1);
        xuatxu.setText("Xuất xứ: " +xuatxu1);
    }

    private void laydulieu() {
        final Intent intent = this.getIntent();
        machitiet = intent.getIntExtra("ma" , 0);
        tenchitiet = intent.getStringExtra("ten");
        giachitiet =  intent.getIntExtra("gia" ,0);
        anhchitiet = intent.getStringExtra("anh");
        motachitiet = intent.getStringExtra("mota");
        soluongchitiet =  intent.getIntExtra("soluong" , 0);
        xuatxu1 = intent.getStringExtra("xuatxu");
        thuonghieu1 = intent.getStringExtra("thuonghieu");

    }
}
