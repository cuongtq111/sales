package com.example.da_banhang;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class shopping_cart_v2 extends AppCompatActivity {

    TextView kiemtragiohang;
    static TextView txt_tonggia;
    Button btnthanhtoan, btntieptucmua;
    ListView list_giohang;
    shopping_cart_adapter adapter ;
    LinearLayout dongsanpham;
    TextView soluongtronggiohang;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart_v2);
        anhxa_giohang();
        tinhtien();

        kiemtragiohang = findViewById(R.id.kiemtragiohang);
        if (trangchu_fragment.array_shoppingcart.size() <= 0 ){
            kiemtragiohang.setText("Giỏ hàng trống!");
        }
        else {
            kiemtragiohang.setVisibility(View.INVISIBLE);

        }
        soluongtronggiohang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btntieptucmua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });
        soluongtronggiohang.setText("Giỏ hàng("+trangchu_fragment.array_shoppingcart.size()+")");
        Intent intent = this.getIntent();
        intent.putExtra("soluong" , soluongtronggiohang.getText().toString());

        btnthanhtoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                startActivity( new Intent(shopping_cart_v2.this ,Login_v2.class));
               startActivity(new Intent(shopping_cart_v2.this , thongtindonhang.class));
            }
        });
    }

    private void xoasanphamtronggiohang() {
        list_giohang.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                builder.setTitle("Xóa hàng trong giỏ!!");
                builder.setIcon(R.drawable.ic_warning_black_24dp);
                builder.setMessage("Bạn chắc xóa chứ??");
                builder.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(trangchu_fragment.array_shoppingcart.size() > 0){
                            trangchu_fragment.array_shoppingcart.remove(position);
                            adapter.notifyDataSetChanged();
                            tinhtien();
                        }
                    }
                });
                builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        adapter.notifyDataSetChanged();
                        tinhtien();
                    }
                });
                builder.show();
                return true;
            }
        });
    }

    public void anhxa_giohang(){
        btntieptucmua = findViewById(R.id.btn_tieptucmua);
        soluongtronggiohang = findViewById(R.id.trangthai);
//        back = findViewById(R.id.back);
        txt_tonggia = findViewById(R.id.txt_total);
        btnthanhtoan = findViewById(R.id.btn_pay);
        dongsanpham  = findViewById(R.id.dongsanpham);
        list_giohang = findViewById(R.id.list_shoppingcart);
        adapter = new shopping_cart_adapter(getApplicationContext(),trangchu_fragment.array_shoppingcart);
        list_giohang.setAdapter(adapter);

    }
    public static  void  tinhtien(){
        long tongtien =  0 ;
        for (int  i = 0 ; i < trangchu_fragment.array_shoppingcart.size() ; i++){
            tongtien += trangchu_fragment.array_shoppingcart.get(i).getGia();
            DecimalFormat format = new DecimalFormat("###,###,###");
            txt_tonggia.setText("Tổng tiền: "+ format.format(tongtien) + "đ");
        }
    }
    public void editsoluong(){

    }
}
