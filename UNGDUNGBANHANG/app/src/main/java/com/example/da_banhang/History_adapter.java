package com.example.da_banhang;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class History_adapter extends ArrayAdapter<Product_share> {
    ArrayList<Product_share> arrayList;
    public History_adapter(@NonNull Context context, @NonNull ArrayList<Product_share> objects) {
        super(context, 0, objects);

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        Product_share furniture = getItem(position);
        if (convertView==null)
        {
            convertView= LayoutInflater.from(getContext()).inflate(R.layout.row_products,parent,false);
        }
        ImageView anh = convertView.findViewById(R.id.img_pro);
        TextView ten = convertView.findViewById(R.id.txt_name_pro);
        TextView gia = convertView.findViewById(R.id.txt_price);

        ten.setText(furniture.name_pro_share);
        gia.setText(furniture.pri_pro_share + "");
        Picasso.with(getContext())
                .load(furniture.img_pro_share)
                .placeholder(R.mipmap.ic_launcher)
                .into(anh);
        return convertView;
    }
}
