package com.example.da_banhang;

import java.util.ArrayList;

public class Product_share {
    int id;
        String img_pro_share;
        int like_pro_share, pri_pro_share;
        String name_pro_share , status_pro_share , mota, xuatxu, thuonghieu;

        public Product_share(){

    }
    static ArrayList<Product_share> History = new ArrayList<>();
    public ArrayList<Product_share> getFurnitureHistory() {
        return this.History;
    }
    public Product_share(String xuatxu, String thuonghieu ,int id ,String img_pro_share, int like_pro_share, int pri_pro_share, String name_pro_share, String status_pro_share, String mota) {
            this.thuonghieu = thuonghieu;
            this.xuatxu = xuatxu;
            this.id = id;
        this.img_pro_share = img_pro_share;
        this.like_pro_share = like_pro_share;
        this.pri_pro_share = pri_pro_share;
        this.name_pro_share = name_pro_share;
        this.status_pro_share = status_pro_share;
        this.mota = mota;
    }

    public String getXuatxu() {
        return xuatxu;
    }

    public void setXuatxu(String xuatxu) {
        this.xuatxu = xuatxu;
    }

    public String getThuonghieu() {
        return thuonghieu;
    }

    public void setThuonghieu(String thuonghieu) {
        this.thuonghieu = thuonghieu;
    }

    public String getMota() {
        return mota;
    }

    public void setMota(String mota) {
        this.mota = mota;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImg_pro_share() {
        return img_pro_share;
    }

    public void setImg_pro_share(String img_pro_share) {
        this.img_pro_share = img_pro_share;
    }

    public int getLike_pro_share() {
        return like_pro_share;
    }

    public void setLike_pro_share(int like_pro_share) {
        this.like_pro_share = like_pro_share;
    }

    public int getPri_pro_share() {
        return pri_pro_share;
    }

    public void setPri_pro_share(int pri_pro_share) {
        this.pri_pro_share = pri_pro_share;
    }

    public String getName_pro_share() {
        return name_pro_share;
    }

    public void setName_pro_share(String name_pro_share) {
        this.name_pro_share = name_pro_share;
    }

    public String getStatus_pro_share() {
        return status_pro_share;
    }

    public void setStatus_pro_share(String status_pro_share) {
        this.status_pro_share = status_pro_share;
    }
}
