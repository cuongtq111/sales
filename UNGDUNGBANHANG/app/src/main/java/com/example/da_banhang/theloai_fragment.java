package com.example.da_banhang;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.view.menu.MenuView;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class theloai_fragment extends Fragment {
    ArrayList<loaisanpham_classs> addarray;
    loaisanpham_adapter rvAdapter;
    RecyclerView recyclerView;
    ProgressDialog mProgressDialog;
    TextView  numcart ;
    ImageView m_anh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.theloai_fragment, container, false);

        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recycle_loaisanpham);
        numcart = view.findViewById(R.id.btn_cart_type);
        numcart.setText("" + trangchu_fragment.array_shoppingcart.size());
        numcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity() , shopping_cart_v2.class));
            }
        });


        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent intent = new Intent(getActivity() , product_forTypepro.class);
                intent.putExtra("maloai", addarray.get(position).getMaloai());
                intent.putExtra("tenloai" ,addarray.get(position).getName());
                intent.putExtra("anhloai", addarray.get(position).getAnhloai());
                loaisanpham_classs item =  addarray.get(position);



                Bundle bundle = new Bundle();
                bundle.putString("anhloai" , addarray.get(position).getAnhloai());
                intent.putExtra("abc" ,bundle);
                ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), new Pair<View, String>(view.findViewById(R.id.img_type),getString(R.string.anh_share)) );
                ActivityCompat.startActivity(getContext() , intent, optionsCompat.toBundle());
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        getdata();

    }


    public void getdata(){
        show_progress();
        String url =Link_page.url_type;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject obj = new JSONObject(response);
                            addarray= new ArrayList<>();
                            JSONArray dataArray  = obj.getJSONArray("loaisanpham");

                            for (int i = 0; i < dataArray.length(); i++) {
                                loaisanpham_classs playerModel = new loaisanpham_classs();
                                JSONObject dataobj = dataArray.getJSONObject(i);
                                playerModel.setMaloai(dataobj.getInt("ma"));
                                playerModel.setName(dataobj.getString("ten"));
                                playerModel.setAnhloai(dataobj.getString("anhloai"));
                                addarray.add(playerModel);
                            }

                            rvAdapter = new loaisanpham_adapter(addarray,getActivity());
                            recyclerView.setAdapter(rvAdapter);
                            tat_load();
                            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });

        // request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        requestQueue.add(stringRequest);
    }
    public void show_progress(){
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Chờ tí...");
        mProgressDialog.setTitle("Đang tải dữ liệu");
        mProgressDialog.setIcon(R.drawable.hourglass);
        mProgressDialog.show();
    }
    public void tat_load(){
        mProgressDialog.dismiss();
    }
    public void loadsanphamtheoloai(){

    }
}
