package com.example.da_banhang;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login_v2 extends AppCompatActivity {
    EditText edituser,editpass;
    Button btnlogin;
    Button register;
    ImageView anh ;
    SharedPreferences sharedPreferences ;
    SharedPreferences.Editor editor ;

     public void shardulieu(){
         sharedPreferences = getSharedPreferences("my_data", MODE_PRIVATE);
         editor = sharedPreferences.edit();
     }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_v2);


        shardulieu();

        edituser  = (EditText) findViewById(R.id.edit_user);
        editpass = (EditText) findViewById(R.id.edit_pass);
        btnlogin = (Button) findViewById(R.id.btnlogin);
        register =  findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext() , Register_v2.class);
                startActivity(intent);

            }
        });
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                login_get();
            }
        });

    }

    public  void login_get(){
        String url = Link_page.login;
        StringRequest request1 = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(Login_v2.this, response.toString(), Toast.LENGTH_SHORT).show();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String sucess = jsonObject.getString("succes");
                    if(sucess.equals("1")){
                        Toast.makeText(Login_v2.this, "Đăng nhập thành công", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext() , thongtindonhang.class);
                        intent.putExtra("name1" , edituser.getText().toString());
                        intent.putExtra("pass" , editpass.getText().toString());
                        startActivity(intent);
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Đăng nhập không thành công", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String > abc = new HashMap<>();
                abc.put("User_Email",edituser.getText().toString());
                abc.put("User_Password" ,editpass.getText().toString());
                return abc;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request1);

    }
}
