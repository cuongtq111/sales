package com.example.da_banhang;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class thongtindonhang extends AppCompatActivity {
    EditText ten;
    EditText Email;
    EditText Diachi, edit_dienthoai;
    Button btnhoanthanh, btncancel ;


    public static boolean isValid(String email)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thongtindonhang);
        anhxa();


        btnhoanthanh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long millis = System.currentTimeMillis();
                java.sql.Date date = new java.sql.Date(millis);
                final String ngayhientai = String.valueOf(date);
                final String tenkh =  ten.getText().toString().trim();
                final String diachikh =  Diachi.getText().toString().trim();
                final String emailkh =  Email.getText().toString().trim();
                final String dienthoai = edit_dienthoai.getText().toString().trim();

                if(isValid(Email.getText().toString())){

                    if(tenkh.length() < 0 && diachikh.length() < 0  && emailkh.length() < 0 && edit_dienthoai.length() < 0){
                        edit_dienthoai.setError("Phải nhập đầy đủ kí tự");
                        ten.setError("Phải nhập đầy đủ kí tự");
                        Email.setError("Phải nhập đầy đủ kí tự");
                        Diachi.setError("Phải nhập đầy đủ kí tự");


                    }
                    else {
                        RequestQueue requestQueue1 = Volley.newRequestQueue(getApplicationContext());
                        StringRequest stringRequest1 = new StringRequest(Request.Method.POST, Link_page.donhang, new Response.Listener<String>() {
                            @Override
                            public void onResponse(final String mahang) {
                                if(Integer.parseInt(mahang) > 0){
                                    RequestQueue queue2 = Volley.newRequestQueue(getApplicationContext());
                                    StringRequest request2 = new StringRequest(Request.Method.POST, Link_page.chitietdonhanh, new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            if(response.equals("1")){

                                                trangchu_fragment.array_shoppingcart.clear();
                                                startActivity(new Intent(thongtindonhang.this, MainActivity.class));

                                                Toast.makeText(thongtindonhang.this, "Mua hành thành công", Toast.LENGTH_SHORT).show();

                                            } else {
                                                Toast.makeText(thongtindonhang.this, "Mua hàng không thành công", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {

                                        }
                                    }){
                                        @Override
                                        protected Map<String, String> getParams() throws AuthFailureError {

                                            JSONArray jsonArray = new JSONArray();
                                            for (int i = 0 ; i < trangchu_fragment.array_shoppingcart.size() ; i++){
                                                JSONObject jsonObject = new JSONObject();
                                                try {
                                                    jsonObject.put("madonhang",mahang);
                                                    jsonObject.put("masanpham",trangchu_fragment.array_shoppingcart.get(i).getMa());
                                                    jsonObject.put("tensanpham",trangchu_fragment.array_shoppingcart.get(i).getTen());
                                                    jsonObject.put("giasanpham", trangchu_fragment.array_shoppingcart.get(i).getGia());
                                                    jsonObject.put("soluong",trangchu_fragment.array_shoppingcart.get(i).getSoluong());
                                                    jsonObject.put("ngaymua",ngayhientai);
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                                jsonArray.put(jsonObject);
                                            }
                                            HashMap<String, String> hass = new HashMap<String, String>();
                                            hass.put("json",jsonArray.toString());
                                            return hass;
                                        }
                                    };
                                    queue2.add(request2);

                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                HashMap<String, String> pagram = new HashMap<String, String>();
                                pagram.put("name", tenkh);
                                pagram.put("mail", emailkh);
                                pagram.put("diachi", diachikh);
                                pagram.put("dienthoai",dienthoai);
                                return pagram;
                            }
                        };
                        requestQueue1.add(stringRequest1);
                    }
                } else {
                    Email.setError("Mail chưa đúng định dạng!!");
                }

            }
        });
        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void  anhxa(){
        btncancel =  findViewById(R.id.btncancel);
        edit_dienthoai = findViewById(R.id.edit_dienthoai);
        ten = findViewById(R.id.editthongtin);
        Email = findViewById(R.id.editmailkhachhang);
        Diachi = findViewById(R.id.editdiachi);
        btnhoanthanh = findViewById(R.id.btnthanhtoanthanhcon);
    }
}
