package com.example.da_banhang;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class loaisanpham_adapter extends RecyclerView.Adapter<loaisanpham_adapter.Viewholder> {

    ArrayList<loaisanpham_classs> arrayList_loaisanpham;
    Context context;

    public loaisanpham_adapter(ArrayList<loaisanpham_classs> arrayList_loaisanpham, Context context) {
        this.arrayList_loaisanpham = arrayList_loaisanpham;
        this.context = context;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_loaisanpham,parent,false);
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.scale_);
        view.startAnimation(animation);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
//        holder.img_loaisanpham.setImageResource(arrayList_loaisanpham.get(position).getAnhloai());
        holder.txt_nameloaisanpham.setText(arrayList_loaisanpham.get(position).getName());
        Picasso.with(context).load(arrayList_loaisanpham.get(position).getAnhloai()).placeholder(R.mipmap.ic_launcher).into(holder.img_loaisanpham);
    }

    @Override
    public int getItemCount() {
        return arrayList_loaisanpham.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder{
        ImageView img_loaisanpham;
        TextView txt_nameloaisanpham;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            img_loaisanpham = itemView.findViewById(R.id.img_type);
            txt_nameloaisanpham = itemView.findViewById(R.id.txt_nametype);

        }
    }
}
