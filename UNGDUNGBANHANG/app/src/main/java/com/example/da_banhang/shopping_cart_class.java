package com.example.da_banhang;

public class shopping_cart_class {
    int gia;
    String ten;
    String anh;
    int soluong;
    int tonggia;
    int ma;

    public shopping_cart_class(int ma , int gia, String ten, String anh, int soluong, int tonggia) {
        this.ma = ma;
        this.gia = gia;
        this.ten = ten;
        this.anh = anh;
        this.soluong = soluong;
        this.tonggia = tonggia;
    }
    public shopping_cart_class(){

    }

    public int getGia() {
        return gia;
    }

    public void setGia(int gia) {
        this.gia = gia;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getAnh() {
        return anh;
    }

    public void setAnh(String anh) {
        this.anh = anh;
    }

    public int getSoluong() {
        return soluong;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }

    public int getTonggia() {
        return tonggia;
    }

    public void setTonggia(int tonggia) {
        this.tonggia = tonggia;
    }

    public int getMa() {
        return ma;
    }

    public void setMa(int ma) {
        this.ma = ma;
    }
}
