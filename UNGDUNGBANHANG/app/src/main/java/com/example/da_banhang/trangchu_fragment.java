package com.example.da_banhang;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;



public class trangchu_fragment extends Fragment{
    Button btnchuyen;
    private  Integer[] image = {R.drawable.panner1,R.drawable.panner5,R.drawable.banner3};
    Timer timer;

    ArrayList<Product_share> addarray;
    ProgressDialog mProgressDialog;
    Adapter_products_share rvAdapter;
    RecyclerView recyclerView;
    TextView btn_cart,btn_allpro;


    //loai sản phẩm
    ArrayList<loaisanpham_classs> addarray1;
    adapter_typehome rvAdapter1;
    RecyclerView recyclerView1;

    //Mảng chứa sản phẩm trong shopping cart
    public static ArrayList<shopping_cart_class> array_shoppingcart;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.trangchu_fragment , container,false);
        btn_cart = (TextView) view.findViewById(R.id.btn_cart);
        ///Đoạn trình chiếu panner sản phẩm
        final ViewPager viewPager = view.findViewById(R.id.viewpaper);
        panner_adapter adapter = new panner_adapter(getActivity(),image);
        viewPager.setAdapter(adapter);

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                viewPager.post(new Runnable(){

                    @Override
                    public void run() {
                        viewPager.setCurrentItem((viewPager.getCurrentItem()+1)%image.length);
                    }
                });
            }
        };
        timer = new Timer();
        timer.schedule(timerTask, 3000, 3000);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.list_product_home);
        recyclerView1 = view.findViewById(R.id.tap2_loai);


        recyclerView1.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView1, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent intent = new Intent(getActivity() , product_forTypepro.class);
                intent.putExtra("maloai", addarray1.get(position).getMaloai());
                intent.putExtra("tenloai", addarray1.get(position).getName());
                intent.putExtra("anhloai", addarray1.get(position).getAnhloai());
                ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), new Pair<View, String>(view.findViewById(R.id.anhtrang_home),getString(R.string.anh_share)) );
                ActivityCompat.startActivity(getContext() , intent, optionsCompat.toBundle());
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        getdatatype();
        btn_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), shopping_cart_v2.class));
            }
        });
        getdata();
        if(array_shoppingcart != null){
            btn_cart.setText(array_shoppingcart.size() +"");
        }
        //Nếu giỏ hàng rỗng thì khởi tạo trạng thái mới cho mảng giỏ hàng
        else
        {
            array_shoppingcart = new ArrayList<>();
            btn_cart.setText(array_shoppingcart.size() +"");
        }

        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Product_share.History.add(addarray.get(position));
                Intent intent = new Intent(getActivity(), Detail_product.class);
                intent.putExtra("ma", addarray.get(position).getId());
                intent.putExtra("ten", addarray.get(position).getName_pro_share());
                intent.putExtra("anh", addarray.get(position).getImg_pro_share());
                intent.putExtra("gia", addarray.get(position).getPri_pro_share());
                intent.putExtra("mota", addarray.get(position).getMota());
                intent.putExtra("soluong", addarray.get(position).getStatus_pro_share());
                intent.putExtra("xuatxu", addarray.get(position).getXuatxu());
                intent.putExtra("thuonghieu", addarray.get(position).getThuonghieu());

                startActivity(intent);

                getActivity().overridePendingTransition(R.anim.in_animatiom,R.anim.out_animation);
            }
            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    public  void getdata(){
        show_progress();
        String url = Link_page.url_product;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        show_progress();
                        try {

                            JSONObject obj = new JSONObject(response);
                                addarray= new ArrayList<>();
                                JSONArray dataArray  = obj.getJSONArray("sanpham");

                                for (int i = 0; i < dataArray.length(); i++) {
                                    Product_share playerModel = new Product_share();
                                    JSONObject dataobj = dataArray.getJSONObject(i);
                                    playerModel.setId(dataobj.getInt("ma"));
                                    playerModel.setMota(dataobj.getString("mota"));
                                    playerModel.setName_pro_share(dataobj.getString("name"));
                                    playerModel.setPri_pro_share(dataobj.getInt("price"));
                                    playerModel.setImg_pro_share(dataobj.getString("img"));
                                    playerModel.setStatus_pro_share(dataobj.getString("soluong"));
                                    playerModel.setXuatxu(dataobj.getString("xuatxu"));
                                    playerModel.setThuonghieu(dataobj.getString("thuonghieu"));
                                    addarray.add(playerModel);

                                }
                                rvAdapter = new Adapter_products_share(addarray,getActivity());
                                recyclerView.setAdapter(rvAdapter);
                                tat_load();
                                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });

        // request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }
    public void show_progress(){
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Chờ tí...");
        mProgressDialog.setTitle("Đang tải dữ liệu");
        mProgressDialog.setIcon(R.drawable.hourglass);
        mProgressDialog.show();
    }
    public void tat_load()
    {
        mProgressDialog.dismiss();
    }
    public void getdatatype(){

        String url =Link_page.url_type;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject obj = new JSONObject(response);
                            addarray1 = new ArrayList<>();
                            JSONArray dataArray  = obj.getJSONArray("loaisanpham");

                            for (int i = 0; i < dataArray.length(); i++) {
                                loaisanpham_classs playerModel1= new loaisanpham_classs();
                                JSONObject dataobj = dataArray.getJSONObject(i);
                                playerModel1.setMaloai(dataobj.getInt("ma"));
                                playerModel1.setName(dataobj.getString("ten"));
                                playerModel1.setAnhloai(dataobj.getString("anhloai"));
                                addarray1.add(playerModel1);
                            }

                            rvAdapter1 = new adapter_typehome(addarray1,getContext());
                            recyclerView1.setAdapter(rvAdapter1);

                            recyclerView1.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });

        // request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        requestQueue.add(stringRequest);
    }
    }

