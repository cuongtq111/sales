package com.example.da_banhang;

public class loaisanpham_classs {
    int maloai;
    private String anhloai;
    private String name;

    public loaisanpham_classs(){

    }
    public loaisanpham_classs(int maloai,String anhloai, String name) {
        this.maloai = maloai;
        this.anhloai = anhloai;
        this.name = name;
    }

    public int getMaloai() {
        return maloai;
    }

    public void setMaloai(int maloai) {
        this.maloai = maloai;
    }

    public String getAnhloai() {
        return anhloai;
    }

    public void setAnhloai(String anhloai) {
        this.anhloai = anhloai;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
